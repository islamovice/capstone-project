import { Project } from "../models/project.model";

export default class ProjectService {
  async createProject({
    userId,
    description,
  }: {
    userId: number;
    image: Express.Multer.File;
    description: string;
  }): Promise<Project> {
    const imagePath = "/public/image/";

    const project = await Project.create({
      userId,
      image: imagePath,
      description,
    });

    return project;
  }

  async getProjects(): Promise<Project[]> {
    const projects = await Project.findAll();
    return projects;
  }

  async getProjectById(id: string): Promise<Project | null> {
    const project = await Project.findByPk(id);
    return project;
  }

  async updateProjectById(
    id: string,
    {
      userId,
      image,
      description,
    }: {
      userId?: number;
      image?: Express.Multer.File;
      description?: string;
    },
  ): Promise<Project | null> {
    const project = await Project.findByPk(id);
    if (!project) {
      return null;
    }

    if (userId) project.userId = userId;
    if (image) project.image = image.path;
    if (description) project.description = description;

    await project.save();
    return project;
  }

  async deleteProjectById(id: string): Promise<boolean> {
    const project = await Project.findByPk(id);
    if (!project) {
      return false;
    }

    await project.destroy();
    return true;
  }
}
