import { Experience } from "../models/experience.model";

export default class ExperienceService {
  async createExperience({
    userId,
    companyName,
    role,
    startDate,
    endDate,
    description,
  }: {
    userId: number;
    companyName: string;
    role: string;
    startDate: Date;
    endDate: Date;
    description: string;
  }): Promise<Experience> {
    const experience = await Experience.create({
      userId,
      companyName,
      role,
      startDate,
      endDate,
      description,
    });
    return experience;
  }

  async getExperiences(): Promise<Experience[]> {
    const experiences = await Experience.findAll();
    return experiences;
  }

  async getExperienceById(id: number): Promise<Experience | null> {
    const experience = await Experience.findByPk(id);
    return experience;
  }

  async updateExperienceById(
    id: number,
    {
      companyName,
      role,
      startDate,
      endDate,
      description,
    }: {
      companyName?: string;
      role?: string;
      startDate?: Date;
      endDate?: Date;
      description?: string;
    },
  ): Promise<Experience | null> {
    const experience = await Experience.findByPk(id);
    if (!experience) {
      return null;
    }

    if (companyName) experience.companyName = companyName;
    if (role) experience.role = role;
    if (startDate) experience.startDate = startDate;
    if (endDate) experience.endDate = endDate;
    if (description) experience.description = description;

    await experience.save();
    return experience;
  }

  async deleteExperienceById(id: number): Promise<boolean> {
    const experience = await Experience.findByPk(id);
    if (!experience) {
      return false;
    }

    await experience.destroy();
    return true;
  }
}
