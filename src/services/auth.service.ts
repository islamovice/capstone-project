import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import { config } from "../config";
import { User, UserRole } from "../models/user.model";

export default class AuthService {
  async generateToken(user: User) {
    const token = jwt.sign(
      { id: user.id, role: user.role },
      config.auth.secret,
      {
        expiresIn: "24h",
      },
    );
    return token;
  }

  async comparePasswords(candidatePassword: string, hashedPassword: string) {
    return bcrypt.compare(candidatePassword, hashedPassword);
  }

  // eslint-disable-next-line class-methods-use-this
  async hashPassword(password: string) {
    // eslint-disable-next-line no-return-await
    return await bcrypt.hash(password, 10);
  }

  async createUser({
    firstName,
    lastName,
    email,
    password,
    title,
    summary,
    image,
  }: {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    title: string;
    summary: string;
    image: Express.Multer.File;
  }): Promise<User> {
    const hashedPassword = await this.hashPassword(password);

    const imagePath = image.path;

    const user = await User.create({
      firstName,
      lastName,
      email,
      password: hashedPassword,
      title,
      summary,
      image: imagePath,
      role: UserRole.User,
    });
    return user;
  }

  async login(
    email: string,
    password: string,
  ): Promise<{ user: User; token: string } | null> {
    const user = await User.findOne({ where: { email } });

    if (!user) {
      return null;
    }

    const passwordMatch = await this.comparePasswords(password, user.password);

    if (!passwordMatch) {
      return null;
    }

    const token = await this.generateToken(user);
    return { user, token };
  }
}
