import { User } from "../models/user.model";
import { Experience } from "../models/experience.model";
import { Project } from "../models/project.model";
import { Feedback } from "../models/feedback.model";

export default class CVService {
  async getCV(userId: number) {
    const user = await User.findByPk(userId, {
      include: [
        {
          model: Experience,
          as: "experiences",
        },
        {
          model: Project,
          as: "projects",
        },
        {
          model: Feedback,
          as: "feedbacks",
        },
      ],
    });
    if (!user) {
      return null;
    }

    const cv = {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      title: user.title,
      image: user.image,
      summary: user.summary,
      email: user.email,
      experiences: user.experiences,
      projects: user.projects,
      feedbacks: user.feedbacks,
    };

    return cv;
  }
}
