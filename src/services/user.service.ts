import bcrypt from "bcrypt";
import { User, UserRole } from "../models/user.model";

export default class UserService {
  async createUserAsAdmin({
    firstName,
    lastName,
    email,
    password,
    title,
    summary,
  }: {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    title: string;
    summary: string;
    image: Express.Multer.File;
  }): Promise<User> {
    const hashedPassword = await bcrypt.hash(password, 10);
    const imagePath = "public/default.png";

    const user = await User.create({
      firstName,
      lastName,
      email,
      password: hashedPassword,
      title,
      summary,
      image: imagePath,
      role: UserRole.User,
    });

    return user;
  }

  async getUsers(): Promise<User[]> {
    const users = await User.findAll();
    return users;
  }

  async getUserById(id: string): Promise<User | null> {
    const user = await User.findByPk(id);
    return user;
  }

  async updateUserById(
    id: string,
    {
      firstName,
      lastName,
      email,
      password,
      title,
      summary,
    }: {
      firstName?: string;
      lastName?: string;
      email?: string;
      password?: string;
      title?: string;
      summary?: string;
    },
  ): Promise<User | null> {
    const user = await User.findByPk(id);
    if (!user) {
      return null;
    }

    if (firstName) user.firstName = firstName;
    if (lastName) user.lastName = lastName;
    if (email) user.email = email;
    if (password) user.password = await bcrypt.hash(password, 10);
    if (title) user.title = title;
    if (summary) user.summary = summary;

    await user.save();
    return user;
  }

  async deleteUserById(id: string): Promise<boolean> {
    const user = await User.findByPk(id);
    if (!user) {
      return false;
    }

    await user.destroy();
    return true;
  }
}
