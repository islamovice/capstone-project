import { Feedback } from "../models/feedback.model";

export default class FeedbackService {
  async createFeedback({
    fromUser,
    companyName,
    toUser,
    context,
  }: {
    fromUser: number;
    companyName: string;
    toUser: number;
    context: string;
  }): Promise<Feedback> {
    const feedback = await Feedback.create({
      fromUser,
      companyName,
      toUser,
      context,
    });
    return feedback;
  }

  async getFeedbacks(): Promise<Feedback[]> {
    const feedbacks = await Feedback.findAll();
    return feedbacks;
  }

  async getFeedbackById(id: number): Promise<Feedback | null> {
    const feedback = await Feedback.findByPk(id);
    return feedback;
  }

  async updateFeedbackById(
    id: number,
    {
      fromUser,
      companyName,
      toUser,
      context,
    }: {
      fromUser?: number;
      companyName?: string;
      toUser?: number;
      context?: string;
    },
  ): Promise<Feedback | null> {
    const feedback = await Feedback.findByPk(id);
    if (!feedback) {
      return null;
    }

    if (fromUser) feedback.fromUser = fromUser;
    if (companyName) feedback.companyName = companyName;
    if (toUser) feedback.toUser = toUser;
    if (context) feedback.context = context;

    await feedback.save();
    return feedback;
  }

  async deleteFeedbackById(id: number): Promise<boolean> {
    const feedback = await Feedback.findByPk(id);
    if (!feedback) {
      return false;
    }

    await feedback.destroy();
    return true;
  }
}
