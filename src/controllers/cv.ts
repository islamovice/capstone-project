import { Request, Response } from "express";
import CVService from "../services/cv.service";

const cvService = new CVService();

export const getCV = async (req: Request, res: Response) => {
  try {
    const userId = parseInt(req.params.id);
    const cv = await cvService.getCV(userId);

    if (!cv) {
      return res.status(404).json({ error: "User not found" });
    }

    return res.status(200).json(cv);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};
