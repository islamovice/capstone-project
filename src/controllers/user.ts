import { Request, Response } from "express";
import UserService from "../services/user.service";

const userService = new UserService();

export const createUser = async (req: Request, res: Response) => {
  try {
    const { firstName, lastName, email, password, title, summary } = req.body;

    const image = req.file;

    const user = await userService.createUserAsAdmin({
      firstName,
      lastName,
      email,
      password,
      title,
      summary,
      image,
    });

    return res.status(201).json({
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      title: user.title,
      summary: user.summary,
      image: user.image,
      role: user.role,
    });
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const getUsers = async (req: Request, res: Response) => {
  try {
    const users = await userService.getUsers();
    return res.status(200).json(users);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const getUserById = async (req: Request, res: Response) => {
  try {
    const userId = req.params.id;
    const user = await userService.getUserById(userId);
    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }
    return res.status(200).json(user);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const updateUserById = async (req: Request, res: Response) => {
  try {
    const userId = req.params.id;
    const { firstName, lastName, email, password, title, summary } = req.body;

    const updatedUser = await userService.updateUserById(userId, {
      firstName,
      lastName,
      email,
      password,
      title,
      summary,
    });

    if (!updatedUser) {
      return res.status(404).json({ error: "User not found" });
    }

    return res.status(200).json(updatedUser);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const deleteUserById = async (req: Request, res: Response) => {
  try {
    const userId = req.params.id;
    const deleted = await userService.deleteUserById(userId);

    if (!deleted) {
      return res.status(404).json({ error: "User not found" });
    }

    return res.status(204).send();
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};
