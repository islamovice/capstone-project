import { Request, Response } from "express";
import ProjectService from "../services/project.service";

const projectService = new ProjectService();

export const createProject = async (req: Request, res: Response) => {
  try {
    const { userId, description } = req.body;

    const image = req.file;

    const project = await projectService.createProject({
      userId,
      image,
      description,
    });

    return res.status(201).json(project);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const getProjects = async (req: Request, res: Response) => {
  try {
    const projects = await projectService.getProjects();
    return res.status(200).json(projects);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const getProjectById = async (req: Request, res: Response) => {
  try {
    const projectId = req.params.id;
    const project = await projectService.getProjectById(projectId);
    if (!project) {
      return res.status(404).json({ error: "Project not found" });
    }
    return res.status(200).json(project);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const updateProjectById = async (req: Request, res: Response) => {
  try {
    const projectId = req.params.id;
    const { userId, image, description } = req.body;

    const updatedProject = await projectService.updateProjectById(projectId, {
      userId,
      image,
      description,
    });

    if (!updatedProject) {
      return res.status(404).json({ error: "Project not found" });
    }

    return res.status(200).json(updatedProject);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const deleteProjectById = async (req: Request, res: Response) => {
  try {
    const projectId = req.params.id;
    const deleted = await projectService.deleteProjectById(projectId);

    if (!deleted) {
      return res.status(404).json({ error: "Project not found" });
    }

    return res.status(204).send();
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};
