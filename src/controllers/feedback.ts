import { Request, Response } from "express";
import FeedbackService from "../services/feedback.service";

const feedbackService = new FeedbackService();

export const createFeedback = async (req: Request, res: Response) => {
  try {
    const { fromUser, companyName, toUser, context } = req.body;

    const feedback = await feedbackService.createFeedback({
      fromUser,
      companyName,
      toUser,
      context,
    });

    return res.status(201).json(feedback);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const getFeedbacks = async (req: Request, res: Response) => {
  try {
    const feedbacks = await feedbackService.getFeedbacks();
    return res.status(200).json(feedbacks);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const getFeedbackById = async (req: Request, res: Response) => {
  try {
    const feedbackId = parseInt(req.params.id);
    const feedback = await feedbackService.getFeedbackById(feedbackId);

    if (!feedback) {
      return res.status(404).json({ error: "Feedback not found" });
    }

    return res.status(200).json(feedback);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const updateFeedbackById = async (req: Request, res: Response) => {
  try {
    const feedbackId = parseInt(req.params.id);
    const { fromUser, companyName, toUser, context } = req.body;

    const updatedFeedback = await feedbackService.updateFeedbackById(
      feedbackId,
      {
        fromUser,
        companyName,
        toUser,
        context,
      },
    );

    if (!updatedFeedback) {
      return res.status(404).json({ error: "Feedback not found" });
    }

    return res.status(200).json(updatedFeedback);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const deleteFeedbackById = async (req: Request, res: Response) => {
  try {
    const feedbackId = parseInt(req.params.id);
    const deleted = await feedbackService.deleteFeedbackById(feedbackId);

    if (!deleted) {
      return res.status(404).json({ error: "Feedback not found" });
    }

    return res.status(204).send();
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};
