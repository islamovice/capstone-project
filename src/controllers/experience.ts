import { Request, Response } from "express";
import ExperienceService from "../services/experience.service";

const experienceService = new ExperienceService();

export const createExperience = async (req: Request, res: Response) => {
  try {
    const { userId, companyName, role, startDate, endDate, description } =
      req.body;
    console.log(req);

    const experience = await experienceService.createExperience({
      userId,
      companyName,
      role,
      startDate,
      endDate,
      description,
    });

    return res.status(201).json(experience);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const getExperiences = async (req: Request, res: Response) => {
  try {
    const experiences = await experienceService.getExperiences();
    return res.status(200).json(experiences);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const getExperienceById = async (req: Request, res: Response) => {
  try {
    const experienceId = parseInt(req.params.id);
    const experience = await experienceService.getExperienceById(experienceId);

    if (!experience) {
      return res.status(404).json({ error: "Experience not found" });
    }

    return res.status(200).json(experience);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const updateExperienceById = async (req: Request, res: Response) => {
  try {
    const experienceId = parseInt(req.params.id);
    const { companyName, role, startDate, endDate, description } = req.body;

    const updatedExperience = await experienceService.updateExperienceById(
      experienceId,
      {
        companyName,
        role,
        startDate,
        endDate,
        description,
      },
    );

    if (!updatedExperience) {
      return res.status(404).json({ error: "Experience not found" });
    }

    return res.status(200).json(updatedExperience);
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};

export const deleteExperienceById = async (req: Request, res: Response) => {
  try {
    const experienceId = parseInt(req.params.id);
    const deleted = await experienceService.deleteExperienceById(experienceId);

    if (!deleted) {
      return res.status(404).json({ error: "Experience not found" });
    }

    return res.status(204).send();
  } catch (error) {
    console.error(error);
    return res.status(505).json({ error: "Internal Server Error" });
  }
};
