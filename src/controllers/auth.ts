import { Request, Response } from "express";
import AuthService from "../services/auth.service";

const authService = new AuthService();

export const createUser = async (req: Request, res: Response) => {
  try {
    const { firstName, lastName, email, password, title, summary } = req.body;
    if (!req.file) {
      return res.status(400).json({ error: "Image file is required" });
    }

    const image = req.file;

    const user = await authService.createUser({
      firstName,
      lastName,
      email,
      password,
      title,
      summary,
      image,
    });

    return res.status(201).json({
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      title: user.title,
      summary: user.summary,
      image: user.image,
      role: user.role,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

export const loginUser = async (req: Request, res: Response) => {
  try {
    const { email, password } = req.body;
    const authResult = await authService.login(email, password);

    if (!authResult) {
      return res.status(401).json({ error: "Invalid credentials" });
    }

    return res.status(200).json({
      user: {
        id: authResult.user.id,
        firstName: authResult.user.firstName,
        lastName: authResult.user.lastName,
        email: authResult.user.email,
        title: authResult.user.title,
        summary: authResult.user.summary,
        image: authResult.user.image,
        role: authResult.user.role,
      },
      token: authResult.token,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};
