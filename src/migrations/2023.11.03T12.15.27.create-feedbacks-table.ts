import { MigrationFn } from "umzug";
import { DataTypes, Sequelize } from "sequelize";

export const up: MigrationFn<Sequelize> = async ({ context }) => {
  await context.getQueryInterface().createTable("feedbacks", {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    fromUser: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      references: {
        model: "users",
        key: "id",
      },
    },
    companyName: {
      type: DataTypes.STRING(128),
      allowNull: false,
    },
    toUser: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
    },
    context: {
      type: DataTypes.STRING(128),
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  });
};

export const down: MigrationFn<Sequelize> = async ({ context }) => {
  await context.getQueryInterface().dropTable("feedbacks");
};
