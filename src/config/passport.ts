import passport from "passport";
import { Strategy as LocalStrategy } from "passport-local";
import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import { Request, Response, NextFunction } from "express";
import { User, UserRole } from "../models/user.model";
import AuthService from "../services/auth.service";
import { config } from "./index";

const authService = new AuthService();

passport.use(
  new LocalStrategy(
    { usernameField: "email", passwordField: "password" },
    async (email, password, done) => {
      try {
        const user = await authService.login(email, password);

        if (!user) {
          return done(null, false, { message: "Invalid email or password" });
        }

        return done(null, user.user);
      } catch (error) {
        return done(error);
      }
    },
  ),
);

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.auth.secret,
};

passport.use(
  new JwtStrategy(jwtOptions, async (payload, done) => {
    try {
      const user = await User.findByPk(payload.id);

      if (!user) {
        return done(null, false);
      }

      return done(null, user);
    } catch (error) {
      return done(error, false);
    }
  }),
);

export const roles =
  (allowedRoles: UserRole[]) =>
  (req: Request, res: Response, next: NextFunction) => {
    const user = req.user as User;
    if (user && allowedRoles.includes(user.role)) {
      next();
    } else {
      res.sendStatus(403); // Forbidden
    }
  };

export default passport;
