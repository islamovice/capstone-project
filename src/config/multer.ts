import multer from "multer";

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, "./public");
  },
  filename(req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.jpg`);
  },
});

export const upload = multer({ storage }); // router

const storageProject = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, "./public/images");
  },
  filename(req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.jpg`);
  },
});

export const uploadProjectImage = multer({ storage: storageProject });
