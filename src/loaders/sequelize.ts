import { Sequelize } from "sequelize";
import { Config } from "../config";

export const loadSequelize = (config: Config): Sequelize =>
  new Sequelize({
    dialect: "mysql",
    ...config.db,
  });
