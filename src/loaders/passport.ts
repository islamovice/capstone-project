import passport from "passport";
import { Loader } from "../interfaces/general";

export const loadPassport: Loader = (app) => {
  app.use(passport.initialize());
};
