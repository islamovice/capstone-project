import ExperienceService from "../services/experience.service";
import { Context } from "../interfaces/general";
import AuthService from "../services/auth.service";
import UserService from "../services/user.service";
import FeedbackService from "../services/feedback.service";
import ProjectService from "../services/project.service";
import CVService from "../services/cv.service";

export const loadContext = async (): Promise<Context> => ({
  services: {
    authService: new AuthService(),
    userService: new UserService(),
    experienceService: new ExperienceService(),
    feedbackService: new FeedbackService(),
    projectService: new ProjectService(),
    cvService: new CVService(),
  },
});
