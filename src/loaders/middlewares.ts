import express from "express";
import { Loader } from "../interfaces/general";
import {
  createUserValidationSchema,
  updateUserValidationSchema,
  getUserByIdValidation,
  deleteUserValidation,
} from "../middleware/validationMiddleware";

export const loadMiddlewares: Loader = (app) => {
  app.use(express.static("public"));
  app.use(createUserValidationSchema);
  app.use(updateUserValidationSchema);
  app.use(getUserByIdValidation);
  app.use(deleteUserValidation);
};
