import { loadMiddlewares } from "./middlewares";
import { loadRoutes } from "./routes";
import express from "express";
import { loadContext } from "./context";
import { loadModels } from "./models";
import { loadSequelize } from "./sequelize";
import { config } from "../config";
import { loadPassport } from "./passport";
import { logger } from "../libs/logger";

export const loadApp = async () => {
  const app = express();
  app.use(express.json());

  const sequelize = loadSequelize(config);

  loadModels(sequelize);

  const context = await loadContext();

  loadPassport(app, context);
  loadMiddlewares(app, context);
  loadRoutes(app, context);

  logger.info("App loaded successfully");

  return app;
};
