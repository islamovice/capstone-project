import express from "express";
import AuthService from "../services/auth.service";
import { User } from "../models/user.model";
import UserService from "../services/user.service";
import { Experience } from "../models/experience.model";
import ExperienceService from "../services/experience.service";
import { Feedback } from "../models/feedback.model";
import FeedbackService from "../services/feedback.service";
import ProjectService from "../services/project.service";
import { Project } from "../models/project.model";
import CVService from "../services/cv.service";

export interface Context {
  services: {
    authService: AuthService;
    userService: UserService;
    experienceService: ExperienceService;
    feedbackService: FeedbackService;
    projectService: ProjectService;
    cvService: CVService;
  };
}

export type RouterFactory = (context: Context) => express.Router;

export type Loader = (app: express.Application, context: Context) => void;

export interface Models {
  user: typeof User;
  experience: typeof Experience;
  feedback: typeof Feedback;
  project: typeof Project;
}
