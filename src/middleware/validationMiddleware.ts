import { body, param, query } from "express-validator";

export const getUsersValidation = [
  query("pageSize").isInt({ min: 1 }),
  query("page").isInt({ min: 1 }),
];

export const getUserByIdValidation = param("id")
  .notEmpty()
  .withMessage("User ID is required");

export const deleteUserValidation = param("id")
  .notEmpty()
  .withMessage("User ID is required");

export const createUserValidationSchema = [
  body("firstName").notEmpty().withMessage("First name is required"),
  body("lastName").notEmpty().withMessage("Last name is required"),
  body("title").notEmpty().withMessage("Title is required"),
  body("summary").notEmpty().withMessage("Summary is required"),
  body("email").isEmail().withMessage("Invalid email format"),
  body("password").notEmpty().withMessage("Password is required"),
  body("role").notEmpty().withMessage("Role is required"),
];

export const updateUserValidationSchema = [
  param("id").isInt({ min: 1 }).withMessage("User ID is required"),
  body("firstName").notEmpty().withMessage("First name is required"),
  body("lastName").notEmpty().withMessage("Last name is required"),
  body("title").notEmpty().withMessage("Title is required"),
  body("summary").notEmpty().withMessage("Summary is required"),
  body("email").isEmail().withMessage("Invalid email format"),
  body("password").notEmpty().withMessage("Password is required"),
  body("role").notEmpty().withMessage("Role is required"),
];
