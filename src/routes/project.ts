import express from "express";
import { RouterFactory } from "../interfaces/general";
import { uploadProjectImage } from "../config/multer";
import {
  createProject,
  getProjects,
  getProjectById,
  updateProjectById,
  deleteProjectById,
} from "../controllers/project"; // Import the controller functions

export const makeProjectRouter: RouterFactory = () => {
  const router = express.Router();

  router.post("/", uploadProjectImage.single("avatar"), createProject);

  router.get("/", getProjects);

  router.get("/:id", getProjectById);

  router.put("/:id", uploadProjectImage.single("avatar"), updateProjectById);

  router.delete("/:id", deleteProjectById);

  return router;
};
