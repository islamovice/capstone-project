import express from "express";
import { RouterFactory } from "../interfaces/general";
import {
  createExperience,
  getExperiences,
  getExperienceById,
  updateExperienceById,
  deleteExperienceById,
} from "../controllers/experience"; // Import the controller functions

export const makeExperienceRouter: RouterFactory = () => {
  const router = express.Router();

  router.post("/", createExperience);

  router.get("/", getExperiences);

  router.get("/:id", getExperienceById);

  router.put("/:id", updateExperienceById);

  router.delete("/:id", deleteExperienceById);

  return router;
};
