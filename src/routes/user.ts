import express from "express";
import { RouterFactory } from "../interfaces/general";
import { upload } from "../config/multer";
import {
  createUser,
  getUsers,
  getUserById,
  updateUserById,
  deleteUserById,
} from "../controllers/user"; // Import the controller functions

export const makeUserRouter: RouterFactory = () => {
  const router = express.Router();

  router.post("/", upload.single("avatar"), createUser);

  router.get("/", getUsers);

  router.get("/:id", getUserById);

  router.put("/:id", upload.single("avatar"), updateUserById);

  router.delete("/:id", deleteUserById);

  return router;
};
