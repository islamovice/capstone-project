import express from "express";
import { RouterFactory } from "../interfaces/general";
import { getCV } from "../controllers/cv";

export const makeCvRouter: RouterFactory = () => {
  const router = express.Router();

  router.get("/:id/cv", getCV);

  return router;
};
