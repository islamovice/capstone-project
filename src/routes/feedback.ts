import express from "express";
import { RouterFactory } from "../interfaces/general";
import {
  createFeedback,
  getFeedbacks,
  getFeedbackById,
  updateFeedbackById,
  deleteFeedbackById,
} from "../controllers/feedback"; // Import the controller functions

export const makeFeedbackRouter: RouterFactory = () => {
  const router = express.Router();

  router.post("/", createFeedback);

  router.get("/", getFeedbacks);

  router.get("/:id", getFeedbackById);

  router.put("/:id", updateFeedbackById);

  router.delete("/:id", deleteFeedbackById);

  return router;
};
