import express from "express";
import { RouterFactory } from "../interfaces/general";
import { upload } from "../config/multer";
import { createUser, loginUser } from "../controllers/auth"; // Import the controller functions

export const makeAuthRouter: RouterFactory = () => {
  const router = express.Router();

  // Registration Endpoint: POST /api/auth/register
  router.post("/register", upload.single("avatar"), createUser);

  // Authentication Endpoint: POST /api/auth/login
  router.post("/login", loginUser);

  return router;
};




