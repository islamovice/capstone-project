import { DataTypes, Model, Optional, Sequelize } from "sequelize";
import { Models } from "../interfaces/general";

interface ProjectAttributes {
  id: number;
  userId: number; // Foreign key referencing the users table
  image: string;
  description: string;
}

export class Project
  extends Model<ProjectAttributes, Optional<ProjectAttributes, "id">>
  implements ProjectAttributes
{
  id: number;

  userId: number;

  image: string;

  description: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    Project.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          type: DataTypes.INTEGER.UNSIGNED,
          allowNull: false, // This field cannot be null
        },
        image: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        description: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
      },
      {
        tableName: "projects",
        underscored: false,
        sequelize,
      },
    );
  }

  static associate(models: Models) {
    Project.belongsTo(models.user, {
      foreignKey: "userId", // Foreign key in the feedbacks table referencing the users table
      as: "user", // Alias for the association, useful for eager loading
    });
  }
}
