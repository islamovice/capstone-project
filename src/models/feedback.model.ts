import { DataTypes, Model, Optional, Sequelize } from "sequelize";
import { Models } from "../interfaces/general";

interface FeedbackAttributes {
  id: number;
  fromUser: number; // Foreign key referencing the users table
  companyName: string;
  toUser: number;
  context: string;
}

export class Feedback
  extends Model<FeedbackAttributes, Optional<FeedbackAttributes, "id">>
  implements FeedbackAttributes
{
  id: number;

  fromUser: number;

  companyName: string;

  toUser: number;

  context: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    Feedback.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        fromUser: {
          type: DataTypes.INTEGER.UNSIGNED,
          allowNull: false, // This field cannot be null
        },
        companyName: {
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        toUser: {
          type: DataTypes.INTEGER.UNSIGNED,
          allowNull: false,
        },
        context: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
      },
      {
        tableName: "feedbacks",
        underscored: false,
        sequelize,
      },
    );
  }

  static associate(models: Models) {
    Feedback.belongsTo(models.user, {
      foreignKey: "fromUser", // Foreign key in the feedbacks table referencing the users table
      as: "user", // Alias for the association, useful for eager loading
    });
  }
}
