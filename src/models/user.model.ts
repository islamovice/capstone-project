import { DataTypes, Model, Optional, Sequelize } from "sequelize";
import { Models } from "../interfaces/general";
import { Experience } from "./experience.model"; // Import your Experience model
import { Feedback } from "./feedback.model"; // Import your Feedback model
import { Project } from "./project.model"; // Import your Project model

export enum UserRole {
  Admin = "Admin",
  User = "User",
}

interface UserAttributes {
  id: number;
  firstName: string;
  lastName: string;
  image: string;
  title: string;
  summary: string;
  role: UserRole;
  email: string;
  password: string;
}

interface UserAssociations {
  experiences?: Experience[]; // Define the association with Experience model
  feedbacks?: Feedback[]; // Define the association with Feedback model
  projects?: Project[]; // Define the association with Project model
}

// eslint-disable-next-line @typescript-eslint/no-unsafe-declaration-merging
export interface User extends UserAttributes, UserAssociations {}

// eslint-disable-next-line @typescript-eslint/no-unsafe-declaration-merging
export class User
  extends Model<UserAttributes, Optional<UserAttributes, "id">>
  implements UserAttributes
{
  id: number;

  firstName: string;

  lastName: string;

  image: string;

  title: string;

  summary: string;

  role: UserRole;

  email: string;

  password: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    User.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        firstName: {
          field: "first_name",
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        lastName: {
          field: "last_name",
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        image: {
          type: new DataTypes.STRING(256),
          allowNull: false,
          defaultValue: "public/default.png",
        },
        title: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        summary: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        role: {
          type: new DataTypes.STRING(50),
          allowNull: false,
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        tableName: "users",
        underscored: true,
        sequelize,
      },
    );
  }

  static associate(models: Models) {
    // Example of how to define a association.
    // User.hasMany(models.experience, {
    //   as: 'experience'
    // });
    User.hasMany(models.experience, {
      as: "experiences",
      foreignKey: "userId",
    });
    User.hasMany(models.feedback, { as: "feedbacks", foreignKey: "toUser" });
    User.hasMany(models.project, { as: "projects", foreignKey: "userId" });
  }
}
