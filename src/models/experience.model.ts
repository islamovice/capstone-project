import { DataTypes, Model, Optional, Sequelize } from "sequelize";
import { Models } from "../interfaces/general";

interface ExperienceAttributes {
  id: number;
  userId: number; // Foreign key referencing the users table
  companyName: string;
  role: string;
  startDate: Date;
  endDate: Date;
  description: string;
}

export class Experience
  extends Model<ExperienceAttributes, Optional<ExperienceAttributes, "id">>
  implements ExperienceAttributes
{
  id: number;

  userId: number; // Foreign key referencing the users table

  companyName: string;

  role: string;

  startDate: Date;

  endDate: Date;

  description: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    Experience.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          type: DataTypes.INTEGER.UNSIGNED,
          allowNull: false, // This field cannot be null
        },
        companyName: {
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        role: {
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        startDate: {
          type: DataTypes.DATE,
          allowNull: false,
        },
        endDate: {
          type: DataTypes.DATE,
          allowNull: false,
        },
        description: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
      },
      {
        tableName: "experiences",
        underscored: true,
        sequelize,
      },
    );
  }

  static associate(models: Models) {
    Experience.belongsTo(models.user, {
      foreignKey: "userId", // Foreign key in the experiences table referencing the users table
      as: "user", // Alias for the association, useful for eager loading
    });
  }
}
