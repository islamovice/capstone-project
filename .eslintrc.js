module.exports = {
  extends: ["plugin:@typescript-eslint/recommended"],
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint"],
  root: true,
  env: {
    node: true,
  },
  ignorePatterns: ["dist"],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
  },
};
