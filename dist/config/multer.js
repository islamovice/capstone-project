"use strict";
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.uploadProjectImage = exports.upload = void 0;
const multer_1 = __importDefault(require("multer"));
const storage = multer_1.default.diskStorage({
  destination(req, file, cb) {
    cb(null, "./public");
  },
  filename(req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.jpg`);
  },
});
exports.upload = (0, multer_1.default)({ storage }); // router
const storageProject = multer_1.default.diskStorage({
  destination(req, file, cb) {
    cb(null, "./public/images");
  },
  filename(req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.jpg`);
  },
});
exports.uploadProjectImage = (0, multer_1.default)({ storage: storageProject });
//# sourceMappingURL=multer.js.map
