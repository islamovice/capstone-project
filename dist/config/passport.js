"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.roles = void 0;
const passport_1 = __importDefault(require("passport"));
const passport_local_1 = require("passport-local");
const passport_jwt_1 = require("passport-jwt");
const user_model_1 = require("../models/user.model");
const auth_service_1 = __importDefault(require("../services/auth.service"));
const index_1 = require("./index");
const authService = new auth_service_1.default();
passport_1.default.use(
  new passport_local_1.Strategy(
    { usernameField: "email", passwordField: "password" },
    (email, password, done) =>
      __awaiter(void 0, void 0, void 0, function* () {
        try {
          const user = yield authService.login(email, password);
          if (!user) {
            return done(null, false, { message: "Invalid email or password" });
          }
          return done(null, user.user);
        } catch (error) {
          return done(error);
        }
      }),
  ),
);
const jwtOptions = {
  jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: index_1.config.auth.secret,
};
passport_1.default.use(
  new passport_jwt_1.Strategy(jwtOptions, (payload, done) =>
    __awaiter(void 0, void 0, void 0, function* () {
      try {
        const user = yield user_model_1.User.findByPk(payload.id);
        if (!user) {
          return done(null, false);
        }
        return done(null, user);
      } catch (error) {
        return done(error, false);
      }
    }),
  ),
);
const roles = (allowedRoles) => (req, res, next) => {
  const user = req.user;
  if (user && allowedRoles.includes(user.role)) {
    next();
  } else {
    res.sendStatus(403); // Forbidden
  }
};
exports.roles = roles;
exports.default = passport_1.default;
//# sourceMappingURL=passport.js.map
