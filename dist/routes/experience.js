"use strict";
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeExperienceRouter = void 0;
const express_1 = __importDefault(require("express"));
const experience_1 = require("../controllers/experience"); // Import the controller functions
const makeExperienceRouter = () => {
  const router = express_1.default.Router();
  router.post("/", experience_1.createExperience);
  router.get("/", experience_1.getExperiences);
  router.get("/:id", experience_1.getExperienceById);
  router.put("/:id", experience_1.updateExperienceById);
  router.delete("/:id", experience_1.deleteExperienceById);
  return router;
};
exports.makeExperienceRouter = makeExperienceRouter;
//# sourceMappingURL=experience.js.map
