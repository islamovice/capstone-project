"use strict";
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeProjectRouter = void 0;
const express_1 = __importDefault(require("express"));
const multer_1 = require("../config/multer");
const project_1 = require("../controllers/project"); // Import the controller functions
const makeProjectRouter = () => {
  const router = express_1.default.Router();
  router.post(
    "/",
    multer_1.uploadProjectImage.single("avatar"),
    project_1.createProject,
  );
  router.get("/", project_1.getProjects);
  router.get("/:id", project_1.getProjectById);
  router.put(
    "/:id",
    multer_1.uploadProjectImage.single("avatar"),
    project_1.updateProjectById,
  );
  router.delete("/:id", project_1.deleteProjectById);
  return router;
};
exports.makeProjectRouter = makeProjectRouter;
//# sourceMappingURL=project.js.map
