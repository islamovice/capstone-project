"use strict";
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeCvRouter = void 0;
const express_1 = __importDefault(require("express"));
const cv_1 = require("../controllers/cv");
const makeCvRouter = () => {
  const router = express_1.default.Router();
  router.get("/:id/cv", cv_1.getCV);
  return router;
};
exports.makeCvRouter = makeCvRouter;
//# sourceMappingURL=cv.js.map
