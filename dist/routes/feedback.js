"use strict";
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeFeedbackRouter = void 0;
const express_1 = __importDefault(require("express"));
const feedback_1 = require("../controllers/feedback"); // Import the controller functions
const makeFeedbackRouter = () => {
  const router = express_1.default.Router();
  router.post("/", feedback_1.createFeedback);
  router.get("/", feedback_1.getFeedbacks);
  router.get("/:id", feedback_1.getFeedbackById);
  router.put("/:id", feedback_1.updateFeedbackById);
  router.delete("/:id", feedback_1.deleteFeedbackById);
  return router;
};
exports.makeFeedbackRouter = makeFeedbackRouter;
//# sourceMappingURL=feedback.js.map
