"use strict";
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeAuthRouter = void 0;
const express_1 = __importDefault(require("express"));
const multer_1 = require("../config/multer");
const auth_1 = require("../controllers/auth"); // Import the controller functions
const makeAuthRouter = () => {
  const router = express_1.default.Router();
  // Registration Endpoint: POST /api/auth/register
  router.post("/register", multer_1.upload.single("avatar"), auth_1.createUser);
  // Authentication Endpoint: POST /api/auth/login
  router.post("/login", auth_1.loginUser);
  return router;
};
exports.makeAuthRouter = makeAuthRouter;
//# sourceMappingURL=auth.js.map
