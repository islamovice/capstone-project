"use strict";
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeUserRouter = void 0;
const express_1 = __importDefault(require("express"));
const multer_1 = require("../config/multer");
const user_1 = require("../controllers/user"); // Import the controller functions
const makeUserRouter = () => {
  const router = express_1.default.Router();
  router.post("/", multer_1.upload.single("avatar"), user_1.createUser);
  router.get("/", user_1.getUsers);
  router.get("/:id", user_1.getUserById);
  router.put("/:id", multer_1.upload.single("avatar"), user_1.updateUserById);
  router.delete("/:id", user_1.deleteUserById);
  return router;
};
exports.makeUserRouter = makeUserRouter;
//# sourceMappingURL=user.js.map
