"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteProjectById =
  exports.updateProjectById =
  exports.getProjectById =
  exports.getProjects =
  exports.createProject =
    void 0;
const project_service_1 = __importDefault(
  require("../services/project.service"),
);
const projectService = new project_service_1.default();
const createProject = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const { userId, description } = req.body;
      const image = req.file;
      const project = yield projectService.createProject({
        userId,
        image,
        description,
      });
      return res.status(201).json(project);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.createProject = createProject;
const getProjects = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const projects = yield projectService.getProjects();
      return res.status(200).json(projects);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.getProjects = getProjects;
const getProjectById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const projectId = req.params.id;
      const project = yield projectService.getProjectById(projectId);
      if (!project) {
        return res.status(404).json({ error: "Project not found" });
      }
      return res.status(200).json(project);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.getProjectById = getProjectById;
const updateProjectById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const projectId = req.params.id;
      const { userId, image, description } = req.body;
      const updatedProject = yield projectService.updateProjectById(projectId, {
        userId,
        image,
        description,
      });
      if (!updatedProject) {
        return res.status(404).json({ error: "Project not found" });
      }
      return res.status(200).json(updatedProject);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.updateProjectById = updateProjectById;
const deleteProjectById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const projectId = req.params.id;
      const deleted = yield projectService.deleteProjectById(projectId);
      if (!deleted) {
        return res.status(404).json({ error: "Project not found" });
      }
      return res.status(204).send();
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.deleteProjectById = deleteProjectById;
//# sourceMappingURL=project.js.map
