"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.loginUser = exports.createUser = void 0;
const auth_service_1 = __importDefault(require("../services/auth.service"));
const authService = new auth_service_1.default();
const createUser = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const { firstName, lastName, email, password, title, summary } = req.body;
      if (!req.file) {
        return res.status(400).json({ error: "Image file is required" });
      }
      const image = req.file;
      const user = yield authService.createUser({
        firstName,
        lastName,
        email,
        password,
        title,
        summary,
        image,
      });
      return res.status(201).json({
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        title: user.title,
        summary: user.summary,
        image: user.image,
        role: user.role,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  });
exports.createUser = createUser;
const loginUser = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const { email, password } = req.body;
      const authResult = yield authService.login(email, password);
      if (!authResult) {
        return res.status(401).json({ error: "Invalid credentials" });
      }
      return res.status(200).json({
        user: {
          id: authResult.user.id,
          firstName: authResult.user.firstName,
          lastName: authResult.user.lastName,
          email: authResult.user.email,
          title: authResult.user.title,
          summary: authResult.user.summary,
          image: authResult.user.image,
          role: authResult.user.role,
        },
        token: authResult.token,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  });
exports.loginUser = loginUser;
//# sourceMappingURL=auth.js.map
