"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUserById =
  exports.updateUserById =
  exports.getUserById =
  exports.getUsers =
  exports.createUser =
    void 0;
const user_service_1 = __importDefault(require("../services/user.service"));
const userService = new user_service_1.default();
const createUser = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const { firstName, lastName, email, password, title, summary } = req.body;
      const image = req.file;
      const user = yield userService.createUserAsAdmin({
        firstName,
        lastName,
        email,
        password,
        title,
        summary,
        image,
      });
      return res.status(201).json({
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        title: user.title,
        summary: user.summary,
        image: user.image,
        role: user.role,
      });
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.createUser = createUser;
const getUsers = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const users = yield userService.getUsers();
      return res.status(200).json(users);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.getUsers = getUsers;
const getUserById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const userId = req.params.id;
      const user = yield userService.getUserById(userId);
      if (!user) {
        return res.status(404).json({ error: "User not found" });
      }
      return res.status(200).json(user);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.getUserById = getUserById;
const updateUserById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const userId = req.params.id;
      const { firstName, lastName, email, password, title, summary } = req.body;
      const updatedUser = yield userService.updateUserById(userId, {
        firstName,
        lastName,
        email,
        password,
        title,
        summary,
      });
      if (!updatedUser) {
        return res.status(404).json({ error: "User not found" });
      }
      return res.status(200).json(updatedUser);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.updateUserById = updateUserById;
const deleteUserById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const userId = req.params.id;
      const deleted = yield userService.deleteUserById(userId);
      if (!deleted) {
        return res.status(404).json({ error: "User not found" });
      }
      return res.status(204).send();
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.deleteUserById = deleteUserById;
//# sourceMappingURL=user.js.map
