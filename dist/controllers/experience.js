"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteExperienceById =
  exports.updateExperienceById =
  exports.getExperienceById =
  exports.getExperiences =
  exports.createExperience =
    void 0;
const experience_service_1 = __importDefault(
  require("../services/experience.service"),
);
const experienceService = new experience_service_1.default();
const createExperience = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const { userId, companyName, role, startDate, endDate, description } =
        req.body;
      console.log(req);
      const experience = yield experienceService.createExperience({
        userId,
        companyName,
        role,
        startDate,
        endDate,
        description,
      });
      return res.status(201).json(experience);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.createExperience = createExperience;
const getExperiences = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const experiences = yield experienceService.getExperiences();
      return res.status(200).json(experiences);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.getExperiences = getExperiences;
const getExperienceById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const experienceId = parseInt(req.params.id);
      const experience =
        yield experienceService.getExperienceById(experienceId);
      if (!experience) {
        return res.status(404).json({ error: "Experience not found" });
      }
      return res.status(200).json(experience);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.getExperienceById = getExperienceById;
const updateExperienceById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const experienceId = parseInt(req.params.id);
      const { companyName, role, startDate, endDate, description } = req.body;
      const updatedExperience = yield experienceService.updateExperienceById(
        experienceId,
        {
          companyName,
          role,
          startDate,
          endDate,
          description,
        },
      );
      if (!updatedExperience) {
        return res.status(404).json({ error: "Experience not found" });
      }
      return res.status(200).json(updatedExperience);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.updateExperienceById = updateExperienceById;
const deleteExperienceById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const experienceId = parseInt(req.params.id);
      const deleted =
        yield experienceService.deleteExperienceById(experienceId);
      if (!deleted) {
        return res.status(404).json({ error: "Experience not found" });
      }
      return res.status(204).send();
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.deleteExperienceById = deleteExperienceById;
//# sourceMappingURL=experience.js.map
