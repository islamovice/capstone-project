"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteFeedbackById =
  exports.updateFeedbackById =
  exports.getFeedbackById =
  exports.getFeedbacks =
  exports.createFeedback =
    void 0;
const feedback_service_1 = __importDefault(
  require("../services/feedback.service"),
);
const feedbackService = new feedback_service_1.default();
const createFeedback = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const { fromUser, companyName, toUser, context } = req.body;
      const feedback = yield feedbackService.createFeedback({
        fromUser,
        companyName,
        toUser,
        context,
      });
      return res.status(201).json(feedback);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.createFeedback = createFeedback;
const getFeedbacks = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const feedbacks = yield feedbackService.getFeedbacks();
      return res.status(200).json(feedbacks);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.getFeedbacks = getFeedbacks;
const getFeedbackById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const feedbackId = parseInt(req.params.id);
      const feedback = yield feedbackService.getFeedbackById(feedbackId);
      if (!feedback) {
        return res.status(404).json({ error: "Feedback not found" });
      }
      return res.status(200).json(feedback);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.getFeedbackById = getFeedbackById;
const updateFeedbackById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const feedbackId = parseInt(req.params.id);
      const { fromUser, companyName, toUser, context } = req.body;
      const updatedFeedback = yield feedbackService.updateFeedbackById(
        feedbackId,
        {
          fromUser,
          companyName,
          toUser,
          context,
        },
      );
      if (!updatedFeedback) {
        return res.status(404).json({ error: "Feedback not found" });
      }
      return res.status(200).json(updatedFeedback);
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.updateFeedbackById = updateFeedbackById;
const deleteFeedbackById = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const feedbackId = parseInt(req.params.id);
      const deleted = yield feedbackService.deleteFeedbackById(feedbackId);
      if (!deleted) {
        return res.status(404).json({ error: "Feedback not found" });
      }
      return res.status(204).send();
    } catch (error) {
      console.error(error);
      return res.status(505).json({ error: "Internal Server Error" });
    }
  });
exports.deleteFeedbackById = deleteFeedbackById;
//# sourceMappingURL=feedback.js.map
