"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCV = void 0;
const cv_service_1 = __importDefault(require("../services/cv.service"));
const cvService = new cv_service_1.default();
const getCV = (req, res) =>
  __awaiter(void 0, void 0, void 0, function* () {
    try {
      const userId = parseInt(req.params.id);
      const cv = yield cvService.getCV(userId);
      if (!cv) {
        return res.status(404).json({ error: "User not found" });
      }
      return res.status(200).json(cv);
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  });
exports.getCV = getCV;
//# sourceMappingURL=cv.js.map
