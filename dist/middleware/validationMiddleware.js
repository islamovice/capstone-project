"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateUserValidationSchema =
  exports.createUserValidationSchema =
  exports.deleteUserValidation =
  exports.getUserByIdValidation =
  exports.getUsersValidation =
    void 0;
const express_validator_1 = require("express-validator");
exports.getUsersValidation = [
  (0, express_validator_1.query)("pageSize").isInt({ min: 1 }),
  (0, express_validator_1.query)("page").isInt({ min: 1 }),
];
exports.getUserByIdValidation = (0, express_validator_1.param)("id")
  .notEmpty()
  .withMessage("User ID is required");
exports.deleteUserValidation = (0, express_validator_1.param)("id")
  .notEmpty()
  .withMessage("User ID is required");
exports.createUserValidationSchema = [
  (0, express_validator_1.body)("firstName")
    .notEmpty()
    .withMessage("First name is required"),
  (0, express_validator_1.body)("lastName")
    .notEmpty()
    .withMessage("Last name is required"),
  (0, express_validator_1.body)("title")
    .notEmpty()
    .withMessage("Title is required"),
  (0, express_validator_1.body)("summary")
    .notEmpty()
    .withMessage("Summary is required"),
  (0, express_validator_1.body)("email")
    .isEmail()
    .withMessage("Invalid email format"),
  (0, express_validator_1.body)("password")
    .notEmpty()
    .withMessage("Password is required"),
  (0, express_validator_1.body)("role")
    .notEmpty()
    .withMessage("Role is required"),
];
exports.updateUserValidationSchema = [
  (0, express_validator_1.param)("id")
    .isInt({ min: 1 })
    .withMessage("User ID is required"),
  (0, express_validator_1.body)("firstName")
    .notEmpty()
    .withMessage("First name is required"),
  (0, express_validator_1.body)("lastName")
    .notEmpty()
    .withMessage("Last name is required"),
  (0, express_validator_1.body)("title")
    .notEmpty()
    .withMessage("Title is required"),
  (0, express_validator_1.body)("summary")
    .notEmpty()
    .withMessage("Summary is required"),
  (0, express_validator_1.body)("email")
    .isEmail()
    .withMessage("Invalid email format"),
  (0, express_validator_1.body)("password")
    .notEmpty()
    .withMessage("Password is required"),
  (0, express_validator_1.body)("role")
    .notEmpty()
    .withMessage("Role is required"),
];
//# sourceMappingURL=validationMiddleware.js.map
