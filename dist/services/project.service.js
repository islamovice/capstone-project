"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
Object.defineProperty(exports, "__esModule", { value: true });
const project_model_1 = require("../models/project.model");
class ProjectService {
  createProject({ userId, description }) {
    return __awaiter(this, void 0, void 0, function* () {
      const imagePath = "/public/image/";
      const project = yield project_model_1.Project.create({
        userId,
        image: imagePath,
        description,
      });
      return project;
    });
  }
  getProjects() {
    return __awaiter(this, void 0, void 0, function* () {
      const projects = yield project_model_1.Project.findAll();
      return projects;
    });
  }
  getProjectById(id) {
    return __awaiter(this, void 0, void 0, function* () {
      const project = yield project_model_1.Project.findByPk(id);
      return project;
    });
  }
  updateProjectById(id, { userId, image, description }) {
    return __awaiter(this, void 0, void 0, function* () {
      const project = yield project_model_1.Project.findByPk(id);
      if (!project) {
        return null;
      }
      if (userId) project.userId = userId;
      if (image) project.image = image.path;
      if (description) project.description = description;
      yield project.save();
      return project;
    });
  }
  deleteProjectById(id) {
    return __awaiter(this, void 0, void 0, function* () {
      const project = yield project_model_1.Project.findByPk(id);
      if (!project) {
        return false;
      }
      yield project.destroy();
      return true;
    });
  }
}
exports.default = ProjectService;
//# sourceMappingURL=project.service.js.map
