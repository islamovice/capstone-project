"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
Object.defineProperty(exports, "__esModule", { value: true });
const experience_model_1 = require("../models/experience.model");
class ExperienceService {
  createExperience({
    userId,
    companyName,
    role,
    startDate,
    endDate,
    description,
  }) {
    return __awaiter(this, void 0, void 0, function* () {
      const experience = yield experience_model_1.Experience.create({
        userId,
        companyName,
        role,
        startDate,
        endDate,
        description,
      });
      return experience;
    });
  }
  getExperiences() {
    return __awaiter(this, void 0, void 0, function* () {
      const experiences = yield experience_model_1.Experience.findAll();
      return experiences;
    });
  }
  getExperienceById(id) {
    return __awaiter(this, void 0, void 0, function* () {
      const experience = yield experience_model_1.Experience.findByPk(id);
      return experience;
    });
  }
  updateExperienceById(
    id,
    { companyName, role, startDate, endDate, description },
  ) {
    return __awaiter(this, void 0, void 0, function* () {
      const experience = yield experience_model_1.Experience.findByPk(id);
      if (!experience) {
        return null;
      }
      if (companyName) experience.companyName = companyName;
      if (role) experience.role = role;
      if (startDate) experience.startDate = startDate;
      if (endDate) experience.endDate = endDate;
      if (description) experience.description = description;
      yield experience.save();
      return experience;
    });
  }
  deleteExperienceById(id) {
    return __awaiter(this, void 0, void 0, function* () {
      const experience = yield experience_model_1.Experience.findByPk(id);
      if (!experience) {
        return false;
      }
      yield experience.destroy();
      return true;
    });
  }
}
exports.default = ExperienceService;
//# sourceMappingURL=experience.service.js.map
