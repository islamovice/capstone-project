"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const config_1 = require("../config");
const user_model_1 = require("../models/user.model");
class AuthService {
  generateToken(user) {
    return __awaiter(this, void 0, void 0, function* () {
      const token = jsonwebtoken_1.default.sign(
        { id: user.id, role: user.role },
        config_1.config.auth.secret,
        {
          expiresIn: "24h",
        },
      );
      return token;
    });
  }
  comparePasswords(candidatePassword, hashedPassword) {
    return __awaiter(this, void 0, void 0, function* () {
      return bcrypt_1.default.compare(candidatePassword, hashedPassword);
    });
  }
  // eslint-disable-next-line class-methods-use-this
  hashPassword(password) {
    return __awaiter(this, void 0, void 0, function* () {
      // eslint-disable-next-line no-return-await
      return yield bcrypt_1.default.hash(password, 10);
    });
  }
  createUser({ firstName, lastName, email, password, title, summary, image }) {
    return __awaiter(this, void 0, void 0, function* () {
      const hashedPassword = yield this.hashPassword(password);
      const imagePath = image.path;
      const user = yield user_model_1.User.create({
        firstName,
        lastName,
        email,
        password: hashedPassword,
        title,
        summary,
        image: imagePath,
        role: user_model_1.UserRole.User,
      });
      return user;
    });
  }
  login(email, password) {
    return __awaiter(this, void 0, void 0, function* () {
      const user = yield user_model_1.User.findOne({ where: { email } });
      if (!user) {
        return null;
      }
      const passwordMatch = yield this.comparePasswords(
        password,
        user.password,
      );
      if (!passwordMatch) {
        return null;
      }
      const token = yield this.generateToken(user);
      return { user, token };
    });
  }
}
exports.default = AuthService;
//# sourceMappingURL=auth.service.js.map
