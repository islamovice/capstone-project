"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
Object.defineProperty(exports, "__esModule", { value: true });
const feedback_model_1 = require("../models/feedback.model");
class FeedbackService {
  createFeedback({ fromUser, companyName, toUser, context }) {
    return __awaiter(this, void 0, void 0, function* () {
      const feedback = yield feedback_model_1.Feedback.create({
        fromUser,
        companyName,
        toUser,
        context,
      });
      return feedback;
    });
  }
  getFeedbacks() {
    return __awaiter(this, void 0, void 0, function* () {
      const feedbacks = yield feedback_model_1.Feedback.findAll();
      return feedbacks;
    });
  }
  getFeedbackById(id) {
    return __awaiter(this, void 0, void 0, function* () {
      const feedback = yield feedback_model_1.Feedback.findByPk(id);
      return feedback;
    });
  }
  updateFeedbackById(id, { fromUser, companyName, toUser, context }) {
    return __awaiter(this, void 0, void 0, function* () {
      const feedback = yield feedback_model_1.Feedback.findByPk(id);
      if (!feedback) {
        return null;
      }
      if (fromUser) feedback.fromUser = fromUser;
      if (companyName) feedback.companyName = companyName;
      if (toUser) feedback.toUser = toUser;
      if (context) feedback.context = context;
      yield feedback.save();
      return feedback;
    });
  }
  deleteFeedbackById(id) {
    return __awaiter(this, void 0, void 0, function* () {
      const feedback = yield feedback_model_1.Feedback.findByPk(id);
      if (!feedback) {
        return false;
      }
      yield feedback.destroy();
      return true;
    });
  }
}
exports.default = FeedbackService;
//# sourceMappingURL=feedback.service.js.map
