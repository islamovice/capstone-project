"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
Object.defineProperty(exports, "__esModule", { value: true });
const user_model_1 = require("../models/user.model");
const experience_model_1 = require("../models/experience.model");
const project_model_1 = require("../models/project.model");
const feedback_model_1 = require("../models/feedback.model");
class CVService {
  getCV(userId) {
    return __awaiter(this, void 0, void 0, function* () {
      const user = yield user_model_1.User.findByPk(userId, {
        include: [
          {
            model: experience_model_1.Experience,
            as: "experiences",
          },
          {
            model: project_model_1.Project,
            as: "projects",
          },
          {
            model: feedback_model_1.Feedback,
            as: "feedbacks",
          },
        ],
      });
      if (!user) {
        return null;
      }
      const cv = {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        title: user.title,
        image: user.image,
        summary: user.summary,
        email: user.email,
        experiences: user.experiences,
        projects: user.projects,
        feedbacks: user.feedbacks,
      };
      return cv;
    });
  }
}
exports.default = CVService;
//# sourceMappingURL=cv.service.js.map
