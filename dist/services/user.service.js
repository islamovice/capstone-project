"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const user_model_1 = require("../models/user.model");
class UserService {
  createUserAsAdmin({ firstName, lastName, email, password, title, summary }) {
    return __awaiter(this, void 0, void 0, function* () {
      const hashedPassword = yield bcrypt_1.default.hash(password, 10);
      const imagePath = "public/default.png";
      const user = yield user_model_1.User.create({
        firstName,
        lastName,
        email,
        password: hashedPassword,
        title,
        summary,
        image: imagePath,
        role: user_model_1.UserRole.User,
      });
      return user;
    });
  }
  getUsers() {
    return __awaiter(this, void 0, void 0, function* () {
      const users = yield user_model_1.User.findAll();
      return users;
    });
  }
  getUserById(id) {
    return __awaiter(this, void 0, void 0, function* () {
      const user = yield user_model_1.User.findByPk(id);
      return user;
    });
  }
  updateUserById(id, { firstName, lastName, email, password, title, summary }) {
    return __awaiter(this, void 0, void 0, function* () {
      const user = yield user_model_1.User.findByPk(id);
      if (!user) {
        return null;
      }
      if (firstName) user.firstName = firstName;
      if (lastName) user.lastName = lastName;
      if (email) user.email = email;
      if (password) user.password = yield bcrypt_1.default.hash(password, 10);
      if (title) user.title = title;
      if (summary) user.summary = summary;
      yield user.save();
      return user;
    });
  }
  deleteUserById(id) {
    return __awaiter(this, void 0, void 0, function* () {
      const user = yield user_model_1.User.findByPk(id);
      if (!user) {
        return false;
      }
      yield user.destroy();
      return true;
    });
  }
}
exports.default = UserService;
//# sourceMappingURL=user.service.js.map
