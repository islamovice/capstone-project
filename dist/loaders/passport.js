"use strict";
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadPassport = void 0;
const passport_1 = __importDefault(require("passport"));
const loadPassport = (app) => {
  app.use(passport_1.default.initialize());
};
exports.loadPassport = loadPassport;
//# sourceMappingURL=passport.js.map
