"use strict";
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadMiddlewares = void 0;
const express_1 = __importDefault(require("express"));
const validationMiddleware_1 = require("../middleware/validationMiddleware");
const loadMiddlewares = (app) => {
  app.use(express_1.default.static("public"));
  app.use(validationMiddleware_1.createUserValidationSchema);
  app.use(validationMiddleware_1.updateUserValidationSchema);
  app.use(validationMiddleware_1.getUserByIdValidation);
  app.use(validationMiddleware_1.deleteUserValidation);
};
exports.loadMiddlewares = loadMiddlewares;
//# sourceMappingURL=middlewares.js.map
