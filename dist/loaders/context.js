"use strict";
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __importDefault =
  (this && this.__importDefault) ||
  function (mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadContext = void 0;
const experience_service_1 = __importDefault(
  require("../services/experience.service"),
);
const auth_service_1 = __importDefault(require("../services/auth.service"));
const user_service_1 = __importDefault(require("../services/user.service"));
const feedback_service_1 = __importDefault(
  require("../services/feedback.service"),
);
const project_service_1 = __importDefault(
  require("../services/project.service"),
);
const cv_service_1 = __importDefault(require("../services/cv.service"));
const loadContext = () =>
  __awaiter(void 0, void 0, void 0, function* () {
    return {
      services: {
        authService: new auth_service_1.default(),
        userService: new user_service_1.default(),
        experienceService: new experience_service_1.default(),
        feedbackService: new feedback_service_1.default(),
        projectService: new project_service_1.default(),
        cvService: new cv_service_1.default(),
      },
    };
  });
exports.loadContext = loadContext;
//# sourceMappingURL=context.js.map
