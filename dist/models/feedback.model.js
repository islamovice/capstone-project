"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Feedback = void 0;
const sequelize_1 = require("sequelize");
class Feedback extends sequelize_1.Model {
  static defineSchema(sequelize) {
    Feedback.init(
      {
        id: {
          type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        fromUser: {
          type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
          allowNull: false, // This field cannot be null
        },
        companyName: {
          type: new sequelize_1.DataTypes.STRING(128),
          allowNull: false,
        },
        toUser: {
          type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
          allowNull: false,
        },
        context: {
          type: sequelize_1.DataTypes.TEXT,
          allowNull: false,
        },
      },
      {
        tableName: "feedbacks",
        underscored: false,
        sequelize,
      },
    );
  }
  static associate(models) {
    Feedback.belongsTo(models.user, {
      foreignKey: "fromUser",
      as: "user", // Alias for the association, useful for eager loading
    });
  }
}
exports.Feedback = Feedback;
//# sourceMappingURL=feedback.model.js.map
