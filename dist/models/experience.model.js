"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Experience = void 0;
const sequelize_1 = require("sequelize");
class Experience extends sequelize_1.Model {
  static defineSchema(sequelize) {
    Experience.init(
      {
        id: {
          type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
          allowNull: false, // This field cannot be null
        },
        companyName: {
          type: new sequelize_1.DataTypes.STRING(128),
          allowNull: false,
        },
        role: {
          type: new sequelize_1.DataTypes.STRING(128),
          allowNull: false,
        },
        startDate: {
          type: sequelize_1.DataTypes.DATE,
          allowNull: false,
        },
        endDate: {
          type: sequelize_1.DataTypes.DATE,
          allowNull: false,
        },
        description: {
          type: sequelize_1.DataTypes.TEXT,
          allowNull: false,
        },
      },
      {
        tableName: "experiences",
        underscored: true,
        sequelize,
      },
    );
  }
  static associate(models) {
    Experience.belongsTo(models.user, {
      foreignKey: "userId",
      as: "user", // Alias for the association, useful for eager loading
    });
  }
}
exports.Experience = Experience;
//# sourceMappingURL=experience.model.js.map
