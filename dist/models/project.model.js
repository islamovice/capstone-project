"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Project = void 0;
const sequelize_1 = require("sequelize");
class Project extends sequelize_1.Model {
  static defineSchema(sequelize) {
    Project.init(
      {
        id: {
          type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
          allowNull: false, // This field cannot be null
        },
        image: {
          type: new sequelize_1.DataTypes.STRING(256),
          allowNull: false,
        },
        description: {
          type: sequelize_1.DataTypes.TEXT,
          allowNull: false,
        },
      },
      {
        tableName: "projects",
        underscored: false,
        sequelize,
      },
    );
  }
  static associate(models) {
    Project.belongsTo(models.user, {
      foreignKey: "userId",
      as: "user", // Alias for the association, useful for eager loading
    });
  }
}
exports.Project = Project;
//# sourceMappingURL=project.model.js.map
